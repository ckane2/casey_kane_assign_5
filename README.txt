Names: Casey Kane
Emails: ckane2@binghamton.edu

----------------------------------------------------------
## To clean:
ant -buildfile src/build.xml clean

----------------------------------------------------------
## To compile:
ant -buildfile src/build.xml all

Please compile before run. Needed to get BUILD and
BUILD/classes directories in directory structure
----------------------------------------------------------
## To run by specifying arguments from command line
ant -buildfile src/build.xml run -Darg0='path/to/input.txt' -Darg1='path/to/output.txt' -Darg2='debug val'

----------------------------------------------------------
## To create tarball for submission
ant -buildfile src/build.xml tarzip

----------------------------------------------------------
## Academic Honesty Statement

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense."

[Date: ] -- 11/22/2017

----------------------------------------------------------
## Algorithm Note

Our tree operates as a standard BST tree where nodes to the right are greater than
the parent, and nodes to the left are less than the parent. It operates in O(log(N))
time, as all BST trees do.

Our logger has 5 levels:
0 - Release: Nothing is printed, this is for running the program fully
1 - Prime: Print if a prime length string is found
2 - Palindrome: print if a plaindrome is found
3 - Visitor: print if element (tree) accepts a visitor
4 - Constructor: print if a constructor is called
----------------------------------------------------------
## Citations

Capitalization function from:
https://stackoverflow.com/questions/1892765/how-to-capitalize-the-first-character-of-each-word-in-a-string

Palindrome check with StringBuilder from:
https://stackoverflow.com/questions/4138827/check-string-for-palindrome
